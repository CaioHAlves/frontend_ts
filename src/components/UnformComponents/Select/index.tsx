import React, { useRef, useEffect, Fragment } from "react";
import ReactSelect, {
  components,
  OptionTypeBase,
  Props as SelectProps,
} from "react-select";
import { useField } from "@unform/core";
import { Typography } from "@material-ui/core";

interface Props extends SelectProps<OptionTypeBase, true> {
  name: string;
}

function getLength(options: any) {
  return options.reduce((acc: number, curr: { options: any }) => {
    if (curr.options) return acc + getLength(curr.options)
    return acc + 1
  }, 0)
}

const Menu = (props: any) => {
  const optionsLength = getLength(props.options)

  return (
    <Fragment>
      <div>
        <Typography className="Total-Equipamentos">{optionsLength + " resultados"}</Typography>
      </div>
      <components.Menu {...props} className="Menu-Search">{props.children}</components.Menu>
    </Fragment>
  )
}

export const Select = ({ name, options, ...rest }: Props) => {
  const selectRef = useRef(null);
  const { fieldName, defaultValue, registerField } = useField(name);

  console.log(defaultValue);

  useEffect(() => {
    registerField({
      name: fieldName,
      ref: selectRef.current,
      // path: "state.value",
      getValue: (ref) => {
        if (rest.isMulti) {
          if (!ref.state.value) {
            return [];
          }
          return ref.state.value.map((option: any) => option.value);
        } else {
          if (!ref.state.value) {
            return "";
          }
          return ref.state.value.value;
        }
      }
    });
  }, [fieldName, registerField, rest.isMulti]);

  return (
    <ReactSelect
      defaultValue={
        defaultValue && options?.find((option) => option.value === defaultValue)
      }
      closeMenuOnSelect={false}
      components={{ Menu }}
      ref={selectRef}
      classNamePrefix="react-select"
      options={options}
      {...rest}
    />
  );
};
export default Select;
